
const progressStartChange = (...data) => {
  return {
    type: 'PROGRESS_START_CHANGE',
    clipName: data.clipName,
    startTime: data.startTime,
    endTime: data.endTime,
  }
}

const progressEndChange = (...data) => {
  return {
    type: 'PROGRESS_END_CHANGE',
    clipName: data.clipName,
    startTime: data.startTime,
    endTime: data.endTime,
  }
}

const changeName = (...data) => {
  return {
    type: 'CHANGE_NAME',
    clipName: data.clipName,
    startTime: data.startTime,
    endTime: data.endTime,
  }
}

const saveClip = (...data) => {
  return {
    type:      'SAVE_CLIP',
    parentId:  data[0],
    clipName:  data[1],
    startTime: data[2],
    endTime:   data[3],
  }
}

const editClip = (...data) => {
  return {
    type: "EDIT_CLIP",
  }
}

const deleteClip = (...data) => {
  return {
    type: "DELETE_CLIP",
    clip: data[0],
    parentId: data[1]
  }
}


export { progressStartChange, progressEndChange, changeName, saveClip, editClip, deleteClip }
