import dataAPI from '../data/video-data.json'

const initialState = {
  dataAPI,
  currentClip: {
    startTime: 0,
    endTime: 0,
    clipName: ''
  }
}

function data(state = initialState, action){
  switch (action.type) {
    case 'ADD_CLIP': {
      let results = []
      if(action.payload.query) {
        const list = state.data.categories[2].playlist
        results = list.filter((item) => {
          return item.author.includes(action.payload.query)
        })
      }
      return {
        ...state,
        search: results
      }
    }
    case 'SAVE_CLIP':
      console.log(action);
        state.dataAPI.videos.map((video) => {
          if(video.id === action.parentId){
              video.clips.push({
                  id        : (parseInt(video.clips.length) + 1).toString(),
                  name      : action.clipName,
                  start_time: action.startTime,
                  end_time  :  action.endTime
              })
          }
        })
        return {
          ...state,
        }
      break
      case 'DELETE_CLIP':
        console.log(action)
        state.dataAPI.videos.map((video) => {
          if(video.id === action.parentId){
            video.clips.map((clip, idx) => {
              if(clip.id === action.clip) video.clips.splice(video.clips.indexOf(idx),1)
            })
          }
        })

        return {
          ...state,
          dataAPI: state.dataAPI
        }
      break
      case 'CHANGE_NAME':
        return {
          ...state,
          currentClip:{
            startTime: action.startTime,
            endTime: action.endTime,
            clipName: action.clipName
          }
        }
      break;
      case 'PROGRESS_START_CHANGE':
        return {
          ...state,
          currentClip:{
            startTime: action.startTime,
            endTime: action.endTime,
            clipName: action.clipName
          }
        }
      break;
      case 'PROGRESS_END_CHANGE':
        return {
          ...state,
          currentClip:{
            startTime: action.startTime,
            endTime: action.endTime,
            clipName: action.clipName
          }
        }
      break;

    default:
      return state

  }
}

export default data
