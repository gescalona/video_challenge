import React, {Component} from 'react'
import {Modal, Button, FormControl} from 'react-bootstrap'
import ProgressBar from './progress-bar'
import {connect} from 'react-redux'
import { progressStartChange, progressEndChange, changeName, saveClip  } from '../../actions/clipAction'
import './modal.css'

class ModalClip extends Component {
  state = {
    startTime: 0,
    endTime: 0,
    duration: 0,
    clipName: '',
    curret: 0
  }
  handleProgressStartChange = event => {

    this.props.dispatch(progressStartChange(this.state.clipName,
                                            this.state.startTime,
                                            this.state.endTime
                                            )
                      )

    this.setState({
      startTime: event.target.value,
      curret: event.target.value,
    })
    this.video.currentTime = event.target.value
  }

  handleProgressEndChange = event => {
    this.props.dispatch(progressEndChange(this.state.clipName,
                                          this.state.startTime,
                                          this.state.endTime
                                          )
                      )

    this.setState({
      endTime: event.target.value,
      curret:  event.target.value,
    })
    this.video.currentTime = event.target.value
  }

  handleLoadedMetaData = (event) => {
    this.video = event.target
    this.setState({
      duration: this.video.duration,
      endTime: this.video.duration,
      curret: this.state.startTime,
    })
  }

  handleNameChange = event => {
    this.props.dispatch(changeName(this.state.clipName,
                                   this.state.startTime,
                                   this.state.endTime
                                  )
                      )
    this.setState({
      clipName: event.target.value
    })
  }

  handleSaveClip = event => {
    this.props.dispatch(saveClip( this.props.videoData.parent_id,
                                  this.state.clipName,
                                  this.state.startTime,
                                  this.state.endTime
                                )
                      )
    this.props.openModal()
  }

  render() {
    return (
      <div className="static-modal">
        <Modal.Dialog>

          <Modal.Header>
            <Modal.Title>{this.props.videoData.name}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
              <video
                controls
                className="VideoModal"
                src={this.props.videoData.src}
                onLoadedMetadata={this.handleLoadedMetaData}
              />

              <div className='title-tag'>Clip name</div>

              <FormControl
                  name='cilp_name'
                  value={this.state.clipName}
                  type="text"
                  label="Text"
                  placeholder="Cilp name"
                  onChange={this.handleNameChange}
                  onKeyUp={this.handleNameChange}
              />

              <div className='title-tag'>Start Time</div>
              <ProgressBar
                duration={this.state.duration}
                value={this.state.startTime}
                handleProgressChange={this.handleProgressStartChange}
              />
              <div className='title-tag'>End Time</div>
              <ProgressBar
                duration={this.state.duration}
                value={this.state.endTime}
                handleProgressChange={this.handleProgressEndChange}
              />

          </Modal.Body>

          <Modal.Footer>
            <Button
              bsStyle='danger'
              onClick={this.props.openModal}
            >
              Close
            </Button>
            <Button
              bsStyle="primary"
              onClick={this.handleSaveClip}
            >
              Save changes
            </Button>
          </Modal.Footer>

        </Modal.Dialog>
      </div>
    )
  }
}
function mapsStateToProps(state, props){
  return {
    data: state.data.dataAPI

  }
}
export default connect(mapsStateToProps)(ModalClip)
