import React, {Component} from 'react'
import './video.css'

class Video extends Component {

  render(){
    const {
      handleLoadedMetaData,
      handleUpdateMetaData,
      handleSeeking,
      handleSeeked
    } = this.props

    return(
      <div className="Video">
        <video
          controls
          autoPlay={this.props.autoplay}
          src={this.props.src + '#t=6,20'}
          ref={this.props.setRef}
          onLoadedMetadata={handleLoadedMetaData}
          onTimeUpdate={handleUpdateMetaData}
        />
      </div>
    )
  }
}

export default Video
