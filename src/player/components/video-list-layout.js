import React from 'react'

const VideoListLayout = (props) => (
    <div>
      {props.children}
    </div>
)

export default VideoListLayout
