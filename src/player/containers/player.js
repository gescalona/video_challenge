import React, {Component} from 'react'
import VideoPlayerView from '../components/video-player-view'
import Video from '../components/video'
import VideoList from './video-list'
import {Grid, Row, Col} from 'react-bootstrap'
import {connect} from 'react-redux'

class Player extends Component {

  handleLoadedMetaData = event => {
  }
  handleUpdateMetaData = event =>{
    this.video = event.target
    this.setState({
      currenttime: (this.video.currentTime)
    })
  }

  handlePlayClip = event => {
    this.clip = event.target.id.split('-')
    let resultado
    let src = ''
    this.props.videos.map((video) =>{
      if(video.id === this.clip[0]){
        src = video.src
        return resultado = video.clips.find( clips => clips.id === this.clip[1] );
      }
    })
    this.video.src = src+'#t=' +resultado.start_time +','+resultado.end_time
    this.video.play()
  }

  setRef = element => {
    this.video = element
  }

  render() {
    return(
      <VideoPlayerView>
        <Grid>
          <Row>
            <Col sm={8}>
              <Video
                autoplay={false}
                src={'https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4'}
                handleLoadedMetaData={this.handleLoadedMetaData}
                handleUpdateMetaData={this.handleUpdateMetaData}
                setRef={this.setRef}
              />
            </Col>
            <Col sm={4}>

              <VideoList
                videos={this.props.videos}
                handlePlayClip={this.handlePlayClip}
              />

            </Col>
            </Row>
        </Grid>
      </VideoPlayerView>
    )
  }
}

function mapStateToProps(state, props){
  return {
    videos: state.data.dataAPI.videos
  }
}

export default connect(mapStateToProps)(Player)
