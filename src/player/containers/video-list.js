import React, { Component } from 'react'
import VideoListLayout from '../components/video-list-layout'
import Modal from '../components/modal'
import '../components/video-list.css'
import {Panel, Table, Button, Glyphicon} from 'react-bootstrap'
import {connect} from 'react-redux'
import { openModal } from '../../actions/modalActions'
import { deleteClip, editClip } from '../../actions/clipAction'

class VideoList extends Component {
  state = {
    openModal: true,
    videoData: {
      src: 'https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4',
      name:'Borrar el initial SRTATE del moda;',
      parent_id: '1'
    }
  }

  handleAddClip = event => {
    this.setState({
      videoData: {
        src: event.target.getAttribute("data-video"),
        name: event.target.getAttribute("data-name"),
        parent_id: event.target.id,
      }
    })
    this.handleOpenModal()
  }

  handleOpenModal = event => {
      this.props.dispatch(openModal())
  }

  handleDeleteClip = clip => {
    this.props.dispatch(deleteClip(clip.target.id.substring(2, clip.target.id.length),
                                   clip.target.getAttribute("data-parent")
                                   )
                        )
    //This is a bad hack, because the list not render the new data
    this.handleOpenModal()
    this.handleOpenModal()
  }

  handleEditClip = event => {
    console.log("edit");
    this.props.dispatch(editClip())
  }

  render() {
    return(
        <VideoListLayout>
        <Panel  bsStyle="primary">
          <Panel.Heading>
            <Panel.Title componentClass="h3">Videos & Clips List</Panel.Title>
          </Panel.Heading>
              {
                this.props.videos.map((item) =>
                    <div className='VideosContainer' key={item.id}>

                      <div className='VideoNameContainer'>

                        <div className='VideoName'>{item.name}</div>

                        <div className='VideoActions' >
                          <Button
                              bsStyle="success"
                              id={item.id}
                              onClick={this.handleAddClip}
                              data-video={item.src}
                              data-name={item.name}
                            >
                            <Glyphicon
                              glyph="plus"
                              id={item.id}
                              onClick={this.handleAddClip}
                              data-video={item.src}
                              data-name={item.name}
                            />
                          </Button>
                        </div>

                      </div>

                      <div className="ClipChildren">
                        <Table striped bordered condensed hover>
                          <tbody>
                            {
                              item.clips.map((clip) => (
                                <tr key={clip.id}>
                                  <td>
                                      {clip.name}
                                  </td>
                                  <td className="text-right">

                                    <Button
                                      key={clip.id}
                                      id={item.id + '-' + clip.id}
                                      bsSize="xsmall"
                                      onClick={this.props.handlePlayClip}
                                    >
                                      <Glyphicon glyph="play" id={item.id + '-' + clip.id} />
                                    </Button>

                                    <Button
                                      key={'e_'+clip.id}
                                      bsStyle="primary"
                                      bsSize="xsmall"
                                      onClick={this.handleEditClip}
                                    >
                                      <Glyphicon glyph="edit" />
                                    </Button>

                                    <Button
                                      key={'d_'+clip.id}
                                      bsStyle="danger"
                                      bsSize="xsmall"
                                      onClick={this.handleDeleteClip}
                                      id={'d_'+clip.id}
                                      data-parent={item.id}
                                    >
                                      <Glyphicon data-parent={item.id} glyph="trash" key={'d_'+clip.id} id={'d_'+clip.id}  />
                                    </Button>

                                  </td>
                                </tr>
                              ))
                            }
                            </tbody>
                        </Table>
                      </div>
                    </div>
                )
              }
              {
                this.props.openModal &&
                  <Modal
                    openModal={this.handleOpenModal}
                    videoData={this.state.videoData}
                    handleSaveClip={this.handleSaveClip}
                  />
              }
          </Panel>
        </VideoListLayout>
      )
  }
}

function mapStateToProps(state, props){
  console.log(props);
  return {
    videos: state.data.dataAPI.videos,
    openModal: state.modal.visibility
  }
}

export default connect(mapStateToProps)(VideoList)
