import React from 'react'
function MainLayout(props) {
  return (
    <section>
      {props.children}
    </section>
  )
}

export default MainLayout
