import React, { Component } from 'react'
import HandleError from '../../errors/containers/handle-errors'
import VideoPlayer from '../../player/containers/player'
import { connect } from 'react-redux'

class Home extends Component {

  render() {
    return(
      <HandleError>
        <VideoPlayer
          videos={this.props.videos}
        />
      </HandleError>
    )
  }
}


function mapStateToProps(state, props) {
  return {
    videos: state.data.videos,
  }
}

export default connect(mapStateToProps)(Home)
