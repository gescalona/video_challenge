import React from 'react'
import { render } from 'react-dom';
import './index.css'
import Home from './home/containers/home'
import {Provider} from 'react-redux'
import { createStore } from 'redux'
import reducer from './reducers/index'

// import data from './data/video-data.json'
import registerServiceWorker from './registerServiceWorker'
const store = createStore(
  reducer,
  {},
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const root = document.getElementById('root')
render(
        <Provider store={store}>
          <Home />
        </Provider>,
        root
      )
registerServiceWorker()
